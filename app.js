var readline = require('readline'),
    socketio = require('socket.io');
 
var readline = require('readline'),
    socketio = require('socket.io-client'),
    util = require('util'),
    color = require("ansi-color").set;
 
var nick;

var socket = socketio.connect('http://localhost:3636');

var rl = readline.createInterface(process.stdin, process.stdout);


rl.question("Please enter a nickname: ", function(name) {

    socket.emit('set name', { type: 'notice', name: name });

    nick = name;

    // rl.close();

});

socket.on('scoreboard', function(usernames) {

  console.log("---- Scoreboard ----");

  for (var user in usernames) {
    console.log(user, ' - ', usernames[user].score);
  }

  console.log("--------------------");

});

socket.on('waiting', function(users) {


  var usernames = users.map(function(user) {
    return user.name;
  });

  // console.log(usernames);

  console.log('wating for', usernames.join(', ') + '...');

});

socket.on('correct guess', function() {
  console.log("Correct!!!");
});

socket.on('result', function(data) {
  for(var user in data.usernames) {
    console.log(user + "\'s guess: " + data.usernames[user].guess);
  }
  console.log("correct word: ", data.word.english);
});

socket.on('word', function(word) {

  console.log(word.spanish);

  rl.question("Your answer: ", function(answer) {

    socket.emit('guess', answer);

  });

});

socket.on('user joined', function(data) {

  if(nick) {
    console.log(data.name + " joined!");
  }

});
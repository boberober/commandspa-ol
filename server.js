var socketio = require('socket.io'),
    fs = require('fs');

// Listen on port 3636
var io = socketio.listen(3636);

var numUsers = 0;

var usernames = {};

var game = {
  word: null
};

var words = JSON.parse(fs.readFileSync('./words.json', {  
  encoding: 'utf-8'
}));

// JSON.parse(words);

io.sockets.on('connection', function (socket) {
  
    // Broadcast a user's message to everyone else in the room
    socket.on('send', function (data) {
        console.log("send!!!", data);
        io.sockets.emit('message', data);
    });
    
    socket.on('set name', function(data) {
      console.log("Setting name:", data);

      socket.username = data.name;

      usernames[socket.username] = {
        name: socket.username,
        score: 0,
        guessed: false
      };

      socket.broadcast.emit('user joined', {
           name: socket.username
      });

      numUsers++;

      // if(Object.keys(usernames).length > 1) {
        startGame();
      // }


    });

    socket.on('disconnect', function() {
      delete usernames[socket.username];

      numUsers--;

      socket.broadcast.emit('user left', {
            name: socket.username,
      });

    })

    socket.on('guess', function(word) {

        usernames[socket.username].guessed = true;
        usernames[socket.username].guess = word;

        var waitingFor = [];

        for(var user in usernames) {
          if(!usernames[user].guessed) {

            waitingFor.push(usernames[user]);
  
          }
        }
        console.log("waiting", waitingFor.length);
        if(waitingFor.length > 0) {

          socket.emit('waiting', waitingFor);

        } else {

          for(var user in usernames) {
            console.log("--------------------------")
            console.log(usernames[user].guess, game.word)
            console.log("-------------------------");
            if( formatWord(usernames[user].guess) === formatWord(game.word.english) ) {
                usernames[user].score++;

                if(user == socket.username) {
                  socket.emit('correct guess');
                }

            }

          }

          io.emit('result', {
            usernames: usernames,
            word: game.word
          });

          logScoreBoard();
          resetGuessed();
          sendWord();

        }

    });

    process.on('SIGINT', function() {
     console.log('Got SIGINT.  Going to exit.');

     socket.disconnect();
     //Your code to execute before process kill.
     process.kill(process.pid);
    });


    function startGame() {

      console.log("Starting game!");

      resetGame();
      logScoreBoard();

      sendWord();

    }

    function resetGame() {

      for(var user in usernames) {
          usernames[user].score = 0;
      }

      resetGuessed();
    }

    function resetGuessed() {

      for(var user in usernames) {
          usernames[user].guessed = false;
          usernames[user].guess = null;
      }

    }

    function sendWord() {

        var selectedIndex = Math.round(Math.random() * words.length -1);
        var word = words[selectedIndex];

        game.word = word;

        console.log(word);
        words.splice(selectedIndex, 1);

        resetGuessed();

        // console.log(typeof words, words.length, words);

        io.emit('word', word);

    } 

    function logScoreBoard() {

      io.emit('scoreboard', usernames);

    }

    function formatWord(word) {
      return word
        .replace(/\(.*\)/, '')
        .trim()
        .toLowerCase()
    }

    function findClientsSocket(roomId, namespace) {
        var res = []
        , ns = io.of(namespace ||"/");    // the default namespace is "/"

        if (ns) {
            for (var id in ns.connected) {
                if(roomId) {
                    var index = ns.connected[id].rooms.indexOf(roomId) ;
                    if(index !== -1) {
                        res.push(ns.connected[id]);
                    }
                } else {
                    res.push(ns.connected[id]);
                }
            }
        }
        return res;
    }

});
